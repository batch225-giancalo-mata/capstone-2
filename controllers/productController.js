// ================ IMPORT/CALL FUNCTIONS ===================



const product = require("../models/product");



// ================ ADD PRODUCTS ===================



module.exports.addProduct = (data) => {
	
	if(data.isAdmin){
	
		let new_product = new product ({
			name: data.product.name,
			description: data.product.description,
			size: data.product.size,
			quantity: data.product.quantity,
			price: data.product.price
		})

		return new_product.save().then((new_product, error) => {
	
			if(error){
				return error
			}

			return {
				message: 'New product created successfully!'
			}
		})
	} 

	let message = Promise.resolve('Access denied. Only admins can perform this action.')

	return message.then((value) => {
		return value

	})
		
};



// ================= GET ALL PRODUCTS ====================



module.exports.getAllProducts = (isAdmin) => {
	
	if(isAdmin){
		return product.find({});
	
	} else {
		return Promise.resolve('Access denied. Only admins can perform this action.');

	}
};



// ================= GET ALL PRODUCTS ====================



module.exports.getProduct = (productId) => {
    
    return product.findById(productId).then(result => {
        return result;
   
    });
};



// ================= UPDATE PRODUCTS ===================



module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
  	
  	if(isAdmin){
	    let updatedProduct = {
		    name: reqBody.name,
		    description: reqBody.description,
		    size: reqBody.size,
		    quantity: reqBody.quantity,
		    price: reqBody.price
	    };
  
    return product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
    
    if (error) {
        return error;
    
    } else {
        return {
          message: "Product updated successfully"
        }
      }
    });

  	} else {
    	return Promise.resolve("Access denied. Only admins can perform this action.");
  	}

};



// =================== ARCHIVE PRODUCTS =====================



module.exports.archiveProduct = (reqParams, isAdmin) => {
	if(isAdmin){
	let updateActiveField = {
		inStock: false
	};

	return product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		
		if (error) {
			return false;
		
		} else {
			return {
				message: "Product marked out-of-stock."
			}
		};
	});

		} else {
	    	return Promise.resolve("Access denied. Only admins can perform this action.");
	  	}

};



// =================== UNARCHIVE PRODUCTS =====================



module.exports.unarchiveProduct = (reqParams, isAdmin) => {

	if(isAdmin){
		let updateActiveField = {
		inStock: true
	};
	
	return product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		
		if (error) {
			return false;
	
		} else {
			return {
				message: "Product is back in-stock."
			}
		};
	});

		} else {
	    	return Promise.resolve("Access denied. Only admins can perform this action.");
	  	}

};



// =================== RETRIEVE ACTIVE PRODUCTS ==================



module.exports.inStock = () => {

	return product.find({inStock: true}).then(result => {
		return result;
	});
};










