// ================== EXPORT FUNCTIONS =====================



const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");



// ================ CREATE PRODUCT ROUTE =====================



router.post("/addProduct", auth.verify, (req, res) => {
	
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});



// =================== RETRIEVE ALL PRODUCTS ROUTE ======================



router.get("/getAllProducts", auth.verify, (req, res) => {
	
	const {isAdmin} = auth.decode(req.headers.authorization);
	
	productController.getAllProducts(isAdmin).then(resultFromController => res.send(resultFromController));

});



// =================== RETRIEVE SINGLE PRODUCT ROUTE ======================



router.get("/getProduct/:productId", (req, res) => {
    
    productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
});



// ================= UPDATE PRODUCT ROUTE ====================



router.put("/updateProduct/:productId", auth.verify, (req, res) => {
  
	const { isAdmin } = auth.decode(req.headers.authorization);
	  
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

});



// ================= ARCHIVE PRODUCT ROUTE ====================



router.put("/archiveProduct/:productId", auth.verify, (req,res) => {
  	
  	const { isAdmin } = auth.decode(req.headers.authorization);
	
	productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));

});



// ================= UNARCHIVE PRODUCT ROUTE ====================



router.put("/unarchiveProduct/:productId", auth.verify, (req,res) => {
  	
  	const { isAdmin } = auth.decode(req.headers.authorization);
	
	productController.unarchiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));

});



// ================== RETRIEVE ACTIVE PRODUCTS ROUTE ===================



router.get("/inStock", (req, res) => {

	productController.inStock().then(resultFromController => res.send(resultFromController));
});






module.exports = router;